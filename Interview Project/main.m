//
//  main.m
//  Interview Project
//
//  Created by Clicklabs118 on 10/8/15.
//  Copyright (c) 2015 Hitesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
