//
//  ViewController.h
//  Interview Project
//
//  Created by Clicklabs118 on 10/8/15.
//  Copyright (c) 2015 Hitesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property(nonatomic,strong)UIView* viewObject;
@property(nonatomic,strong)UINavigationBar* navigationBar;
@property(nonatomic,strong)UITextField* firstNameTextField;
@property(nonatomic,strong)UITextField* lastNameTextField;
@property(nonatomic,strong)UITextField* emailTextField;
@property(nonatomic,strong)UITextField* phoneNumberTextField;
@property(nonatomic,strong)UILabel* personalInfoLabel;
@property(nonatomic,strong)UILabel* registerLabel;
@property(nonatomic,strong)UIView* lineView;
@property(nonatomic,strong)UIButton* registerButton;
@property(nonatomic,strong)UIButton* changePasswordButton;
@property(nonatomic,strong)UIImageView* imageViewObject;
@property(nonatomic,strong)UIImageView* pictureViewObject;

@end

