//
//  ViewController.m
//  Interview Project
//
//  Created by Clicklabs118 on 10/8/15.
//  Copyright (c) 2015 Hitesh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize viewObject,lineView,navigationBar,firstNameTextField,lastNameTextField,emailTextField,phoneNumberTextField,personalInfoLabel,registerLabel,registerButton,changePasswordButton,imageViewObject,pictureViewObject;

- (void)viewDidLoad
{
    [super viewDidLoad];
    viewObject = [UIView new];
    [viewObject setFrame:self.view.frame];
    [viewObject setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_iphone4.png"]]];
    [self.view addSubview:viewObject];
    
    [self setNavigationBarOnView];
    [self createTextField];
    [self createLabel];
    [self createLineView];
    [self createButton];
    [self setButtonImageOnNavigationBar];
    [self createImageView];
    }
-(void)setNavigationBarOnView
{
    
    navigationBar = [UINavigationBar new];
    [navigationBar setFrame:CGRectMake(0, 0, 360, 60)];
    [navigationBar setUserInteractionEnabled:true];
    [navigationBar setBackgroundColor:[UIColor greenColor]];
    [navigationBar setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"back_button.png"]]];
    [viewObject addSubview:navigationBar];
    
}
-(void)setButtonImageOnNavigationBar
{
    imageViewObject=[UIImageView new];
    [imageViewObject setFrame:CGRectMake(10, 20, 35, 40)];
    [imageViewObject setImage:[UIImage imageNamed:@"back_button.png"]];
    [imageViewObject setUserInteractionEnabled:YES];
    [navigationBar addSubview:imageViewObject];
}
-(void)createTextField
{
    firstNameTextField =[UITextField new];
    [firstNameTextField setFrame:CGRectMake(10, 285, 300, 44)];
    [firstNameTextField setUserInteractionEnabled:YES];
    [firstNameTextField setPlaceholder:@"STEVE"];
    [firstNameTextField setTextColor:[UIColor grayColor]];
    [viewObject addSubview:firstNameTextField];
    
    lastNameTextField =[UITextField new];
    [lastNameTextField setFrame:CGRectMake(10,335, 300, 44)];
    [lastNameTextField setUserInteractionEnabled:YES];
    [lastNameTextField setPlaceholder:@"STEPHEN"];
    [lastNameTextField setTextColor:[UIColor grayColor]];
    [viewObject addSubview:lastNameTextField];
    
    emailTextField =[UITextField new];
    [emailTextField setFrame:CGRectMake(10, 385, 300, 44)];
    [emailTextField setUserInteractionEnabled:YES];
    [emailTextField setPlaceholder:@"STEVE.STEPHEN@YAHOOMAIL.COM"];
    [emailTextField setTextColor:[UIColor darkGrayColor]];
    [viewObject addSubview:emailTextField];
    
    phoneNumberTextField =[UITextField new];
    [phoneNumberTextField setFrame:CGRectMake(10,435 , 300, 44)];
    [phoneNumberTextField setUserInteractionEnabled:YES];
    [phoneNumberTextField setPlaceholder:@"+1-444-242-6565"];
    [phoneNumberTextField setKeyboardType:UIKeyboardTypeNumberPad];
    [phoneNumberTextField setTextColor:[UIColor darkGrayColor]];
    [viewObject addSubview:phoneNumberTextField];
    
}
-(void)createLabel;
{
    personalInfoLabel =[UILabel new];
    [personalInfoLabel setFrame:CGRectMake(10, 235, 200, 44)];
    [personalInfoLabel setUserInteractionEnabled:YES];
    [personalInfoLabel setText:@"Personal Info"];
    [personalInfoLabel setTextColor:[UIColor darkGrayColor]];
    [viewObject addSubview:personalInfoLabel];
    
    registerLabel = [UILabel new];
    [registerLabel setFrame:CGRectMake(10,29,navigationBar.frame.size.width-70,14)];
    registerLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    registerLabel.text = @"EDIT PROFILE";
    registerLabel.backgroundColor = [UIColor clearColor];
    registerLabel.font = [UIFont systemFontOfSize:19];
    [registerLabel setTextAlignment:NSTextAlignmentCenter];
    [navigationBar addSubview:registerLabel];
    
    
}
-(void)createLineView
{
    lineView = [UIView new];
    [lineView setFrame:CGRectMake(123, 255, 197, 1)];
    [lineView setBackgroundColor:[UIColor darkGrayColor]];
    [lineView setUserInteractionEnabled:YES];
    [viewObject addSubview:lineView];

}
-(void)createButton
{
    registerButton = [UIButton new];
    [registerButton setFrame:CGRectMake(10, 495, 300, 44)];
    [registerButton setTitle:@"Register" forState:UIControlStateNormal];
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerButton setBackgroundColor:[UIColor darkGrayColor]];
    [registerButton setUserInteractionEnabled:YES];
    [viewObject addSubview:registerButton];
    
    changePasswordButton = [UIButton new];
    [changePasswordButton setFrame:CGRectMake(10, 535, 300, 44)];
    [changePasswordButton setTitle:@"CHANGE PASSWORD" forState:UIControlStateNormal];
    [changePasswordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [changePasswordButton setBackgroundColor:[UIColor clearColor]];
    [changePasswordButton setUserInteractionEnabled:YES];
    [viewObject addSubview:changePasswordButton];
}
-(void)createImageView
{
    pictureViewObject = [UIImageView new];
    [pictureViewObject setFrame:CGRectMake(94, 85, 113, 113)];
    [pictureViewObject setUserInteractionEnabled:YES];
    [pictureViewObject setImage:[UIImage imageNamed:@"Example.png"]];
    [viewObject addSubview:pictureViewObject];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

@end
